import Web.Scotty
import Data.Monoid ((<>))

main :: IO ()
main = do
  scotty 8080 $ do
    get "/" $ do
      text ("Hello Scotty")
    get "/getparam/:whatever" $ do
      what <- param "whatever"
      text ("Hey you passed " <> what <> "!!!")
