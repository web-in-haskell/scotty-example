import Web.Scotty
import Data.Monoid ((<>))
import UserData

allUsers :: [User]
allUsers = [one, two, three, four]

main :: IO ()
main = do
  scotty 8080 $ do
    get "/" $ do
      text ("Hello Scotty")
    get "/getparam/:whatever" $ do
      what <- param "whatever"
      text ("Hey you passed " <> what <> "!!!")
    get "/users" $ do
      json allUsers
    post "/users" $ do
      user <- jsonData :: ActionM User
      json user
    get "/users/:id" $ do
      id <- param "id"
      json (filter (matchesId id) allUsers)
