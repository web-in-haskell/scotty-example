{-# LANGUAGE DeriveGeneric #-}

module UserData where
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics

data User = User {
  userId :: Int,
  userName :: String,
  userAge :: Int
} deriving (Show, Generic)

instance FromJSON User
instance ToJSON User

one :: User
one = User {
  userId = 1,
  userName = "onename",
  userAge = 23
}

two :: User
two = User 2 "twoname" 12

three :: User
three = User 3 "threename" 33

four :: User
four = User 4 "fourname" 44

matchesId :: Int -> User -> Bool
matchesId id user = userId user == id
